% Author: Andreu Gimenez Bolinches <esdandreu@gmail.com>
% Adapted from Felix Duvallet's RI thesis template:
% https://github.com/felixduvallet/ri-thesis-template
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{src/keio-master-thesis}

\LoadClassWithOptions{book}

%% Default packages setup -----------------------------------------------------
% Appendix
\RequirePackage{appendix}
% Colors
\RequirePackage{xcolor}  % Get extra colours.
\colorlet{documentLinkColor}{blue}
\colorlet{documentCitationColor}{black!80}
\definecolor{headergray}{rgb}{0.5,0.5,0.5}
% Hyperlinks
\RequirePackage[
    pageanchor=true,
    plainpages=false,
    pdfpagelabels,
    bookmarks,
    bookmarksnumbered,
]{hyperref}
\hypersetup{
    colorlinks = true,
    citecolor = documentCitationColor,
    linkcolor = documentLinkColor,
    urlcolor = documentLinkColor,
}
% Figures
\RequirePackage{graphicx}
% Fancy headers
\RequirePackage{fancyhdr}
% Helvetica (Arial) font
\RequirePackage{helvet}

%% Header styling -------------------------------------------------------------

% Remove rulers around the chapter titles
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.0pt}

% headers
\fancyhead[LO,R]{\hyperlink{contents}{\slshape \leftmark}}
\fancyhead[LO,R]{\hyperlink{contents}{\slshape \leftmark}}
% Make chapter header line: N. <name>
\renewcommand{\chaptermark}[1]{%
    \markboth{%
        \color{headergray}{%
            \thechapter.\ #1%
        }%
    }{}%
}

% footers
\fancyfoot[C]{}

%% Page styles ----------------------------------------------------------------

\fancypagestyle{plain}{%
    \fancyhf{}
    \fancyfoot[RO,LE]{\thepage}
}

\fancypagestyle{thesis}{%
    \fancyhead{}
    \fancyhead[RO,LE]{\hyperlink{contents}{\slshape \leftmark}}
    \fancyfoot[RO,LE]{\thepage}%
}

\setlength{\headheight}{15pt}
\addtolength{\topmargin}{-3pt}

%% Title ----------------------------------------------------------------------

% Parameters:
% Required
\def\title#1{\gdef\@title{#1}}
\def\author#1{\gdef\@author{#1}}
\def\authorID#1{\gdef\@authorID{#1}}
\def\advisor#1{\gdef\@advisor{#1}}
\def\institute#1{\gdef\@institute{#1}}
\def\fieldOfStudy#1{\gdef\@fieldOfStudy{#1}}
\def\specialisation#1{\gdef\@specialisation{#1}}
\def\date#1{\gdef\@date{#1}}
\def\Year#1{\gdef\@Year{#1}}

\def\maketitle{
    \thispagestyle{empty}
    
    % calculate skip needed to ensure that title appears in the cut-out
    \newlength{\@cutoutvskip}
    \setlength{\@cutoutvskip}{2.1875 true in}       % position of cut-out
    \addtolength{\@cutoutvskip}{-1 true in}         % printer margin
    \addtolength{\@cutoutvskip}{-\topmargin}
    \addtolength{\@cutoutvskip}{-\headheight}
    \addtolength{\@cutoutvskip}{-\headsep}
    
    %% Centered things on the title page must be *physically* centered
    %% on the page, so they line up with the cut-out window. So we hardwire
    %% the margins for the title page so that left margin = right margin:
    %%         left margin = (8.5in - textwidth)/2
    \oddsidemargin=8.5in
    \advance\oddsidemargin by -\textwidth
    \oddsidemargin=.5\oddsidemargin
    \advance\oddsidemargin by -1in % TeX sux
    \let\footnoterule\relax
    \vglue\@cutoutvskip
    
    % Warsaw University of Technology diploma thesis format

    % School
    \begin{center}
        \includegraphics[width=\textwidth]{src/title/logo.jpg}
        \fontsize{12}{14} \selectfont \@institute \par
        \includegraphics[width=\textwidth]{src/title/master.jpg}
        in the field of study \@fieldOfStudy \par
        and specialisation \@specialisation \par
    \end{center}
    
    % Thesis title
    \vfill
    \begin{center}
        \begin{minipage}[t]{.8\textwidth}
            \vfill
            \begin{center}
                {\fontsize{14}{16} \selectfont \@title \par}
            \end{center}
            \vfill
        \end{minipage}
    \end{center}
    
    % Author
    \vfill
    \begin{center}
        {\fontsize{21}{24} \selectfont \@author \par} 
        {\fontsize{12}{14} \selectfont student record book number {\@authorID}}
    \end{center}
    \vspace{1em}
    
    % Advisor
    \vfill
    \begin{center}
        {\fontsize{12}{14} \selectfont thesis supervisor \par \@advisor}
    \end{center}
    
    % Date
    \vfill
    \begin{center}
        {\fontsize{12}{14} \selectfont Warsaw, \@Year \par}
    \end{center}

    \clearpage
}

%% Preface

\newenvironment{dedication}
{
    \thispagestyle{fancy}
    \vspace*{\stretch{1}} \begin{center} \em
        }
        {
    \end{center} \vspace*{\stretch{3}} \clearpage
}

\newenvironment{pseudochapter}[1]{
    \thispagestyle{fancy}
    %%\vspace*{\stretch{1}}
    \begin{center} \large {\bf #1} \end{center}
    \begin{quotation}
    }{
    \end{quotation}
    \vspace*{\stretch{3}}
    \clearpage
}

\newenvironment{abstract}{
    \begin{pseudochapter}{\@title}}{\end{pseudochapter}
}

\newenvironment{acknowledgments}{
    \begin{pseudochapter}{Acknowledgments}}{\end{pseudochapter}
}

\newenvironment{funding}{
    \begin{pseudochapter}{Funding}}{\end{pseudochapter}
}
